import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import { Provider } from 'react-redux'
import createBrowserHistory from 'history/createBrowserHistory'
import createStore from './Redux'

const store = createStore()

export const history = createBrowserHistory()

ReactDOM.render(
  <Provider store={store}>
    <App history={history}/>
  </Provider>
  , document.getElementById('root')
);
registerServiceWorker();
