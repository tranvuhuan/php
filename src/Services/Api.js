import apisauce from 'apisauce'

import Config from '../Config'
import { ConnectableObservable } from 'reduxsauce';

// our "constructor"
const create = (baseURL = 'https://demo.voolatech.com/sunrise/api/public') => {
    // ------
    // STEP 1
    // ------
    //
    // Create and configure an apisauce-based api object.
    //
    const api = apisauce.create({
        // base URL is read from the "constructor"
        baseURL,
        // here are some default headers
        headers: {
          'Content-Type': 'application/json'
        },
        // 10 second timeout...
        timeout: 10000
    })

    api.addAsyncRequestTransform(request => async () => {
        const { token } = Config.GLOBAL_VAR
        if (token) {
            request.headers['Authorization'] = `Bearer ${token}`
        }
        if (request.url.indexOf('/upload-creative-image') > -1) {
            request.headers['Content-Type'] = 'multipart/form-data'
        }
    })

    // ------
    // STEP 2
    // ------
    //
    // Define some functions that call the api.  The goal is to provide
    // a thin wrapper of the api layer providing nicer feeling functions
    // rather than "get", "post" and friends.
    //
    // I generally don't like wrapping the output at this level because
    // sometimes specific actions need to be take on `403` or `401`, etc.
    //
    // Since we can't hide from that, we embrace it by getting out of the
    // way at this level.
    // //

    const getUserInfo = () => api.get('/api/users')
    
    const getPollInfo  = () => api.post('/api/load/polls')

    const getUserActivity = () => api.get('/api/user/activity')

    return {

        getUserInfo,
        getPollInfo,
        getUserActivity,

    }

}

export default {
    create
}
