const STORAGE_KEY_AUTH = 'auth'

export const setAuthStorage = (authData) => {
    return localStorage.setItem(STORAGE_KEY_AUTH, authData)
}

export const getAuthStorage = () => {
    return localStorage.getItem(STORAGE_KEY_AUTH)
}

export const clearAuthStorage = () => {
    return localStorage.removeItem(STORAGE_KEY_AUTH)
}
