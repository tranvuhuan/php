export function formatThousandPrice(price = 0) {
  return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
}

export function formatFloatNumber(val) {
  let num = val.toString().replace(/[^0-9.]/g, '');
  const tempSplit = num.split('.');
  if (tempSplit.length === 1 ||
      (tempSplit.length === 2 && tempSplit[1] === '')) {
      return num;
  }
  num = `${tempSplit[0]}.${tempSplit[1].slice(0, 2)}`;
  return parseFloat(num);
}

export function formatIntNumber(val) {
  return val.toString().replace(/[^0-9]/g, '')
}

export function isEqualTwoObjects(obj1, obj2) {
  return JSON.stringify(obj1) === JSON.stringify(obj2)
}
