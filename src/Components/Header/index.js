import React, {Component} from 'react'
import './Style.css'
import {connect} from 'react-redux'
import UserAction from '../../Redux/UserRedux'

class Header extends React.Component{
  componentDidMount() {
    this.props.getUser()
  }

  render(){
    // console.log(this.props)
    return(
      <header className="header-left">
        <div className="head">
          <div className="show-menu">
          </div>
          <div className="box-avatar">
            <div className="thumb">
              <img src={this.props.user.fullAvatar} alt=""/>
            </div>
            <div className="second circle">
            </div>
            <div className="name">{this.props.user.name}</div>
          </div>
          <div className="toolbar">
            <ul className="list">
              <li>
                <a
                  role="button"
                  title=""
                  onClick={this.userLogout}
                  className="btn-logout" >
                  <span className="material-icons">&#xE879;</span>
                  <span className="text"> đăng xuất
                  </span>
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div className="control">
          <div className="point">
            <div className="num"></div>
            <div className="text">

            </div>
          </div>
          <div className="navigation">

          </div>
          <div className="connect-account">

          </div>
        </div>
      </header>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({
  ...ownProps,
  loading: state.auth.loading,
  user: state.auth.user,
})

const mapDispatchToProps = (dispatch) => ({
  getUser: () => dispatch(UserAction.getUser()),
})

export default connect(mapStateToProps, mapDispatchToProps)(Header)
