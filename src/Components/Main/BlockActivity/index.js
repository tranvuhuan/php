import React, {Component} from 'react'
import './style.css';
import Activity from '../../../Redux/ActivityRedux'
import UserAction from '../../../Redux/UserRedux'
import {connect} from 'react-redux'
import { isIterable } from 'core-js';
import TimeAgo from 'react-timeago';
class BlockActivity extends React.Component{
  constructor(props){
    super(props)
    this.state={
      array: []
    }
    const array = [...this.props.activity]
  }
  componentDidMount() {
    this.props.getActivity();
    this.props.getUser();
    
  }

  render(){
    console.log(this.props.activity)
    return(
      <div className="detail">
        <div className="head-detail my-activity">
          <div className="title">
            <span className="icon icon-activities"></span>
            <span className="text">
              Hoạt động của tôi
            </span>
          </div>
        </div>
        
        <div className="content-detail">
          <ul className="list-noti">
            {
              this.props.activity.map((item, index)=>{
                return(

                  <li key={index}>
                    <div className="decs">
                      <a role="button" title="" >
                        {/* {
                          this.props.user.map(item =>
                          <span>{item.name}</span>)
                        } */}
                      </a>
                      <span className="time">
                        <TimeAgo date={item.created_at} minPeriod={60}/>
                      </span>
                      <p>
                        {item.title}
                      </p>
                    </div>
                  </li>
                )
              })
            }
          </ul>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({
  ...ownProps,
  loading: state.activitys.loading,
  activity: state.activitys.activity,
  user: state.auth.user,
})

const mapDispatchToProps = (dispatch) => ({
  getActivity: () => dispatch(Activity.getActivity()),
  getUser: () => dispatch(UserAction.getUser())
})
export default connect(mapStateToProps, mapDispatchToProps)(BlockActivity)
