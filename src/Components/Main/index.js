import React, {Component} from 'react'
import './style.css';
import PollAction from '../../Redux/Pollredux'
import {connect} from 'react-redux'
import BlockPoll from './BlockPoll/index';
import BlockActivity from './BlockActivity/index'
class Main extends React.Component{

  componentDidMount() {
    this.props.getPoll()
  }
  
  render(){


    return(
        <main className="main">
          <div className="box-title">
            <div className="inner">
              <div className="toolbar">
                <ul>
                  <li>
                    <a title="notification">
                      <span className="material-icons">&#xE7F4;</span>
                      <span className="has-notification"></span>
                    </a>
                  </li>
                  <li>
                    <div className="wrap-country-select">
                      <div className="country-view">
                        <strong>vi</strong>
                        <i className="fa fa-caret-down"/>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
              <div className="title">
                <span className="material-icons"></span>
                <h2>Trang Chu</h2>
              </div>
            </div>
          </div>
          <div>
        <div><BlockPoll/></div>
        <div>< BlockActivity/></div>
      </div>
      </main>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({
  ...ownProps,
  loading: state.polls.loading,
  poll: state.polls.poll,
})

const mapDispatchToProps = (dispatch) => ({
  getPoll: () => dispatch(PollAction.getPoll()),
})

export default connect(mapStateToProps, mapDispatchToProps)(Main)
