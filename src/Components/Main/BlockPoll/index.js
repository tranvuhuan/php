import React, {Component} from 'react'
import './style.css';
import PollAction from '../../../Redux/Pollredux'
import {connect} from 'react-redux'
import { isIterable } from 'core-js';
import TimeAgo from 'react-timeago';
class BlockPoll extends React.Component{
  constructor(props){
    super(props)
    this.state= {
      arraylist: []
    }
    
  }

  componentDidMount() {
    this.props.getPoll()
    
  }
  
  render(){
     console.log(this.props.poll)
    return(
      <div>
        {
          this.props.poll.map((item, index)=>{
            return(
              <div className="detail" key={index}>   
                <div className="head-detail">
                  <div className="title">
                    <div className="avatar">
                      <img src={item.user.fullAvatar} alt={item.user.name}/>
                    </div>
                    <div className="name-time">
                      <span className="text name">{item.user.name}</span>
                      <div className="time">
                        <TimeAgo date={item.created_at} minPeriod={60}/>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="content-detail">
                  <div className="caption">
                    {item.question.question}
                  </div>
                </div>
                <div className="social">
                  {
                    <ul className="list-icons">
                      {
                        <li>
                          <a
                            title="vote"
                          >
                            <span className="material-icons">&#xE80D;</span>
                            <span className="text">
                              đã bỏ phiếu
                            </span>
                          </a>
                        </li>
                      }
                    </ul> 
                  }
                </div>
              </div>
            )
          })
        }
      </div>

    )
  }
}

const mapStateToProps = (state, ownProps) => ({
  ...ownProps,
  loading: state.polls.loading,
  poll: state.polls.poll,
  user: state.auth.user,
})

const mapDispatchToProps = (dispatch) => ({
  getPoll: () => dispatch(PollAction.getPoll()),
})

export default connect(mapStateToProps, mapDispatchToProps)(BlockPoll)
