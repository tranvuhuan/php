import React, { Component } from  'react'
import {connect} from 'react-redux'
import UserAction from './Redux/UserRedux'
import PollAction from './Redux/Pollredux'
import logo from './logo.svg';
import './App.css';

import Header from './Components/Header'
import Main from './Components/Main'

class App extends Component {
  render() {
  
    return (
      <div className="App">

        < Header />
        < Main />
      </div>
    );
  }
}

export default App
