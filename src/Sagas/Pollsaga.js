import { call, put, all } from 'redux-saga/effects'
import { map, reduce } from 'lodash'
import PollActions from '../Redux/Pollredux'

export function* getPollInfo(api){
  const response = yield call(api.getPollInfo)
  
  //  console.log("reponse", response)

  if(response.status === 200){
    yield put(PollActions.getPollSuccess(response.data.data.data))
  }else{
    yield put(PollActions.getPollError(response.data))
  }
}