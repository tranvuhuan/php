import {
  all,
  fork,
  takeLatest,
} from 'redux-saga/effects'

import API from '../Services/Api'

import {
  UserTypes
} from '../Redux/UserRedux'

import {getUserInfo} from './UserSaga'


import {
  PollTypes
} from '../Redux/Pollredux'


import {getPollInfo} from './Pollsaga'

import {
  ActivityTypes
} from '../Redux/ActivityRedux'

import {getUserActivity} from  './UserActivity'
// import {
//   PollTypes
// } from '../Redux/Pollredux'

// import {getPollInfo} from './Pollsaga'
/* ------------- API ------------- */
// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
const api = API.create()

/* ------------- Connect Types To Sagas ------------- */
export default function * root () {
  yield all([
      takeLatest([UserTypes.GET_USER], getUserInfo, api),
      takeLatest([PollTypes.GET_POLL], getPollInfo, api),
      takeLatest([ActivityTypes.GET_ACTIVITY],getUserActivity, api)
  ])
}
