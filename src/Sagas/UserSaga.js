import { call, put, all } from 'redux-saga/effects'
import { map, reduce } from 'lodash'
import UsersActions from '../Redux/UserRedux'

export function* getUserInfo(api) {

  const response = yield call(api.getUserInfo)
  
  if (response.status === 200) {
    yield put(UsersActions.getUserSuccess(response.data.data))
  } else {
    yield put(UsersActions.getUserError(response.data))
  }
}