import { call, put, all } from 'redux-saga/effects'
import { map, reduce } from 'lodash'
import Activitys from '../Redux/ActivityRedux'

export function* getUserActivity(api){

  const response = yield call(api.getUserActivity)

  //console.log("responsive",  response.data.data.data)

  if(response.status===200){
    yield put(Activitys.getActivitySuccess(response.data.data.data))
  }else{
    yield put(Activitys.getActivityError(response.data))
  }
}