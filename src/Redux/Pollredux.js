import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
const { Types, Creators } = createActions({
  getPoll : null,
  getPollSuccess : ['data'],
  getPollError : ['Error']

})

export const PollTypes = Types
export default Creators

/* ------------- Initial State ------------- */
const INITIAL_STATE = Immutable({
  poll : [],
  loading : false,
  error : null,
})

/* ------------- Reducers ------------- */
  const getPoll = state => 
    state.merge({loading: true})

  const getPollSuccess = (state, {data}) =>{
    
    return state.merge({loading: false, poll: data})
     
  }
    
  const getPollError = (state, { error }) =>
    state.merge({ loading: false, error })
  
/* ------------- Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_POLL]: getPoll,
  [Types.GET_POLL_SUCCESS]: getPollSuccess,
  [Types.GET_POLL_ERROR]: getPollError,
})