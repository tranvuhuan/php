import { createStore, applyMiddleware, compose } from 'redux'
import createSagaMiddleware from 'redux-saga'

export default (rootReducer, rootSaga) => {
    const middleware = []
    const enhancers = []

    /* ------------- Saga Middleware ------------- */
    const sagaMiddleware = createSagaMiddleware()
    middleware.push(sagaMiddleware)

    /* ------------- Assemble Middleware ------------- */
    const composeEnhancers = process.env.NODE_ENV === 'production' ? compose :
        (window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose);
    enhancers.push(applyMiddleware(...middleware))
    const store = createStore(rootReducer, composeEnhancers(...enhancers))

    // kick off root saga
    let sagasManager = sagaMiddleware.run(rootSaga)

    return {
        store,
        sagasManager,
        sagaMiddleware
    }
}
