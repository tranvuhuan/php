import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

const { Types, Creators } = createActions({
  getActivity: null,
  getActivitySuccess: ['data'],
  getActivityError: ['error'],

})

export const ActivityTypes = Types
export default Creators

/* ------------- Initial State ------------- */
const INITIAL_STATE = Immutable({
  activity: [],
  loading: false,
  error: null,  
})

/* ------------- Reducers ------------- */
  const getActivity = state =>
    state.merge({loading: true})

  const getActivitySuccess = (state, {data}) =>{
    
    return(
      state.merge({loading: false, activity: data})
    )
  }
    

  const getActivityError = (state, {error}) =>
    state.merge({loading: false, error})

/* ------------- Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_ACTIVITY] : getActivity,
  [Types.GET_ACTIVITY_SUCCESS]: getActivitySuccess,
  [Types.GET_ACTIVITY_ERROR] : getActivityError
})