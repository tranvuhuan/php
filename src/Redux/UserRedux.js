import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

const { Types, Creators } = createActions({
  getUser: null,
  getUserSuccess: ['data'],
  getUserError: ['error'],
})

export const UserTypes = Types
export default Creators

/* ------------- Initial State ------------- */
const INITIAL_STATE = Immutable({
  user: {},
  loading: false,
  error: null,
})

/* ------------- Reducers ------------- */
const getUser = state =>
  state.merge({ loading: true })

const getUserSuccess = (state, { data }) => {
  
  return state.merge({ loading: false, user: data })
}

const getUserError = (state, { error }) =>
  state.merge({ loading: false, error })

/* ------------- Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_USER]: getUser,
  [Types.GET_USER_SUCCESS]: getUserSuccess,
  [Types.GET_USER_ERROR]: getUserError,
})